---
layout: post
title: "[Rencontre] L' Autre 68 : Juin"
subtitle: "Rencontre/conférence à Marennes (Poitou-Charentes) avec Francis Cousin et James Bryant les 28 et 29 avril."
image: /img/actualites/2018-04-04-header.jpg
---

> Plus d'information sur l'évènement sur notre page [Facebook](https://www.facebook.com/events/1704573236272053/) !


- Pourquoi a surgi la radicalité de mai-juin 68 sur le terrain de la fin de la reconstruction qui a suivi la seconde guerre mondiale?

- Pourquoi cette insurrection n'a pu que retomber dans la phase historique concernée.

- À quelles conditions cette contestation sociale radicale pourra t-elle renaître dans le temps qui vient?
