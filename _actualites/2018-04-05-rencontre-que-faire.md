---
layout: post
title: "[Rencontre] Que Faire ?"
subtitle: "Rencontre le 5 mai à Nantes. Au programme notamment Le Militantisme stade Suprême de l'aliénation et Au-delà du parti."
image: /img/actualites/2018-04-05-header.jpg
---

> Plus d'information sur l'évènement sur notre page [Facebook](https://www.facebook.com/events/1577556518958916/) !

En Domination réelle du Capital, la question centrale qui est posée par nombre de gens, et qui a elle même été posée dans le célèbre ouvrage de Lénine c'est : `Que Faire?`

Bien qu'il y réponde d'une manière totalement arbitraire et subjective, la question en elle même reste intéressante et il s'agit justement de bien tirer les leçons de l'histoire sur le syndicalisme et les partis politiques : bref toute organisation qui viendrait briser l'immédiat de la révolution prolétarienne. (Voir au delà du parti par le collectif Junius.)

Dans `La société du spectacle` (thèses 117 à 123), Debord a lui même donné quelques clés sur la constitution de groupes critique en attendant la prochaine crise :

> Dans le pouvoir des Conseils, qui doit supplanter internationalement tout autre pouvoir, le mouvement prolétarien est son propre produit, et ce produit est le producteur même. Il est à lui-même son propre but. Là seulement la négation spectaculaire de la vie est niée son tour.


> L'apparition des Conseils fut la réalité la plus haute du mouvement prolétarien dans le premier quart de siècle, réalité qui resta inaperçue ou travestie parce qu'elle disparaissait avec le reste du mouvement que l'ensemble de l'expérience historique d'alors démentait et éliminait. Dans le nouveau moment de la critique prolétarienne, ce résultat revint comme le seul point invaincu du mouvement vaincu. La conscience historique qui sait qu'elle a en lui son seul milieu d'existence peut le reconnaître maintenant, non plus à la périphérie de ce qui reflue, mais au centre de ce qui monte.


> Une organisation révolutionnaire existant avant le pouvoir des Conseils - elle devra trouver en luttant sa propre forme - pour toutes ces raisons historiques sait déjà qu'elle ne représente pas la classe. Elle doit seulement se reconnaître elle-même comme une séparation radicale d'avec le monde de la séparation.


> L'organisation révolutionnaire est l'expression cohérente de la théorie de la praxis entrant en communication non-unilatérale avec les luttes pratiques, en devenir vers la théorie pratique. Sa propre pratique est la généralisation de la communication et la cohérence dans ces luttes. Dans le moment révolutionnaire de la dissolution de la séparation sociale, cette organisation doit reconnaître sa propre dissolution en tant qu'organisation séparée.

> L'organisation révolutionnaire ne peut être que la critique unitaire de la société, c'est-à-dire une critique qui ne pactise avec aucune forme de pouvoir séparé, en aucun point du monde, et une critique prononcée globalement contre tous les aspects de la vie sociale aliénée. Dans la lutte de l'organisation révolutionnaire contre la société de classes, les armes ne sont pas autre chose que l'essence des combattants mêmes : l'organisation révolutionnaire ne peut reproduire en elle les conditions de scission et de hiérarchie qui sont celles de la société dominante. Elle doit lutter en permanence contre sa déformation dans le spectacle régnant. La seule limite de la participation à la démocratie totale de l'organisation révolutionnaire est la reconnaissance et l'auto-appropriation effective, par tous ses membres, de la cohérence de sa critique, cohérence qui doit se prouver dans la théorie critique proprement dite et dans la relation entre celle-ci et l'activité pratique.


> Quand la réalisation toujours plus poussée de l'aliénation capitaliste à tous les niveaux, en rendant toujours plus difficile aux travailleurs de reconnaître et de nommer leur propre misère, les place dans l'alternative de refuser la totalité de leur misère, ou rien, l'organisation révolutionnaire a dû apprendre qu'elle ne peut plus combattre l'aliénation sous des formes aliénées.

> La révolution prolétarienne est entièrement suspendue à cette nécessité que, pour la première fois, c'est la théorie en tant qu'intelligence de la pratique humaine qui doit être reconnue et vécue par les masses. Elle exige que les ouvriers deviennent dialecticiens et inscrivent leur pensée dans la pratique ; ainsi elle demande aux hommes sans qualité bien plus que la révolution bourgeoise ne demandait aux hommes qualifiés qu'elle déléguait à sa mise en oeuvre : car la conscience idéologique partielle édifiée par une partie de la classe bourgeoise avait pour base cette partie centrale de la vie sociale, l'économie, dans laquelle cette classe était déjà au pouvoir. Le développement même de la société de classes jusqu'à l'organisation du spectaculaire de la non-vie mène donc le projet révolutionnaire à devenir visiblement ce qu'il était déjà essentiellement.


> La théorie révolutionnaire est maintenant ennemie de toute idéologie révolutionnaire, et elle sait qu'elle l'est.


Au-delà de ces compréhensions historiques déterminées, la question reste entière quant à notre époque et les actions qu'elle mérite de mener. Que celles-ci ne se retrouvent pas systématiquement engluées dans le côté purement réactionnaire et organisationnel primaire, mais au contraire, devront chercher à trouver les actes les plus adaptés qui soient.

Aufhebung propose une rencontre / débat sur le sujet afin que ceux qui sont intéressés par la question puissent venir s'exprimer et ainsi obtenir des pistes de "Faire" pratique dans une époque qui voudrait nous voir seulement ne rien faire du tout, ou sinon faire des choses stupides.

Au préalable: seront acceptées les personnes d'une humilité certaine, prêtes à faire un effort de compréhension Historique. Les questionnements arbitraires envahissantes n'ont rien à faire dans des assemblées sérieuses, car "les plus grands sujets ne peuvent jamais être conjecturés au hasard" (Héraclite).

Toute intelligence critique est donc la bienvenue, pour apporter un peu de contradiction dans les compréhensions.

Au programme, débats de compréhension sur différentes lectures :

- Le militantisme stade Suprême de l'aliénation
- AU delà du parti (collectif Junius)



