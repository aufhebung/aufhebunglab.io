---
layout: post
title: "La dictature démocratique du capital dans sa domination réelle"
author: Collectif Aufhebung
important: true
image: /img/actualites/2018-10-08-header.jpg
---

Vous avez été nombreux à nous écrire suite à la rencontre de Nantes en septembre dernier.

Dans une volonté de continuer sur cette lancée, nous proposons **une seconde rencontre à Bordeaux** sur le thème de la Démocratie pure du Capital, qui aura lieu le **samedi 3 Novembre 2018**.

Nous espérons que vous viendrez vous exprimer avec force, joie et perspicacité.

Pour les inscriptions, [cliquez ici](https://docs.google.com/forms/d/e/1FAIpQLSfwVe7oCHp7LPd3-Cnmb9fOT2seR4ZxzlcbWz0nYjBwpuMHlw/viewform) !

Vive la Commune !

<div class="video-container">
    <iframe
        src="https://www.youtube-nocookie.com/embed/Kw8oO1m-bzo"
        frameborder="0"
        allow="autoplay; encrypted-media"
        allowfullscreen>
    </iframe>
</div>
