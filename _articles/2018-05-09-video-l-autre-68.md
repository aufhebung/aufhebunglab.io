---
layout: post
title: "L'Autre 68 : Juin"
subtitle: "Vidéo de la rencontre avec Francis Cousin et James Bryant du 28 et 29 avril."
author: "Collectif Aufhebung"
year: 2018
month: 05
day: 09
published: false
---

<div class="youtube-video">
    <iframe width="560" height="315"
        src="https://www.youtube-nocookie.com/embed/v3xU0lwOjHk?rel=0"
        frameborder="0" allowfullscreen>
    </iframe>
</div>

<div class="youtube-video">
    <iframe width="560" height="315"
        src="https://www.youtube-nocookie.com/embed/fdlzXRdS_LY?rel=0"
        frameborder="0" allowfullscreen>
    </iframe>
</div>

<div class="youtube-video">
    <iframe width="560" height="315"
        src="https://www.youtube-nocookie.com/embed/MssSLxTtTNA?rel=0"
        frameborder="0" allowfullscreen>
    </iframe>
</div>
