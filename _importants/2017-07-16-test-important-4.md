---
title: Pourquoi lire Marx aujourd'hui ?
subtitle: Une conférence/débat organisée à Nantes
author: "Collectif Aufhebung"
year: 2017
month: 07
day: 13
layout: post
image: img/importants/2017-07-13-test-important-1.jpeg
---

**Contrairement au puritanisme borné** des scientifiques payés depuis des siècles pour nous décrire 
les sociétés primitives comme d’atroces monstruosités - comme sociétés bestiales, non encore 
humaines, à l’image du barbare traînant « sa femme » par les cheveux de « la guerre du feu » - 
le matérialisme historique, lui, analyse ces communautés primitives comme étant des communautés 
du **naturalisme**, c’est à dire comme étant **le communisme primitif**. 
