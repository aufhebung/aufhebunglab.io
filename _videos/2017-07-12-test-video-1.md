---
title: Test Video 1
author: "Collectif Aufhebung"
year: 2017
month: 07
day: 12
layout: post
---

**Contrairement au puritanisme borné** des scientifiques payés depuis des siècles pour nous décrire 
les sociétés primitives comme d’atroces monstruosités - comme sociétés bestiales, non encore 
humaines, à l’image du barbare traînant « sa femme » par les cheveux de « la guerre du feu » - 
le matérialisme historique, lui, analyse ces communautés primitives comme étant des communautés 
du **naturalisme**, c’est à dire comme étant **le communisme primitif**. 
